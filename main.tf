 #VARIABLES
##################################################################################

#variable "aws_access_key" {}
#variable "aws_secret_key" {}
#variable "private_key_path" {}
#variable "key_name" {}
variable "region" {
  default = "us-east-1"
}

##################################################################################
# PROVIDERS
##################################################################################

provider "aws" {
  
  region     = var.region
}

##################################################################################
#Secret
##################################################################################
resource "aws_secretsmanager_secret" "example_1" {
  name = "example_1"
}